<?php
    require_once(ROOT .  "/Models/User.php");
    require_once(ROOT .  "/Models/Medic.php");
    require_once(ROOT .  "/Models/Consultation.php");
    require_once(ROOT .  "/Models/Patient.php");
    require_once(ROOT .  "/Controllers/Controller.php");

    class medicController extends Controller{
        
        function index(){
            $this->render("index.html");
        }


        function consultHistory(){
            $user = new Medic("", "", "", "", "", "");
            $consultDao = new Consultation("", "", "", "", "");
    
            $consults = $consultDao->getByMedic($user->getByUsername($_SESSION['login'])->getId());

            $patient = new Patient("", "", "", "", "", "", "");
            foreach ($consults as $consult) {
                $consult->setPatient($patient->get($consult->getPatient())->getName());
                $consult->setDate(str_replace("T", " ", $consult->getDate()));
            }

            $this->set("consults", $consults);

            $this->render("consultHistory.html");
            
        }

        function formConsultUpdate($id){
            $this->set("username", $_SESSION['login']);
            $patientDao = new Patient("", "", "", "", "", "", "");

            $patients = $patientDao->getAll();

            $consult = new Consultation("", "", "", "", "");
            $consult = $consult->get($id);

            foreach ($consult->getTags() as $key => $value) {
                $this->set($key, $value);
            }
            
            
            $this->set("patients", $patients);
            $this->render("formConsultUpdate.html");
        }

        function updateConsult($id){
            $medic = new Medic("", "", "", "", "", "");
            $medic = $medic->getByUsername($_SESSION['login'])->getId();
            $patient = $_POST['patient'];
            $recipe = $_POST['recipe'];
            $obs = $_POST['obs'];
            $date = $_POST['date'];

            $consult = new Consultation($date, $medic, $patient, $recipe, $obs);
            $consult->update($id);

            header("Location: " . WEBROOT . "medic/index");

        }

        function formConsult(){
            $this->set("username", $_SESSION['login']);
            $patientDao = new Patient("", "", "", "", "", "", "");

            $patients = $patientDao->getAll();

            $this->set("patients", $patients);

            $this->render("formConsult.html");
        }

        function registerConsult(){
            $medic = new Medic("", "", "", "", "", "");
            $medic = $medic->getByUsername($_SESSION['login'])->getId();
            $patient = $_POST['patient'];
            $recipe = $_POST['recipe'];
            $obs = $_POST['obs'];
            $date = $_POST['date'];

            $consult = new Consultation($date, $medic, $patient, $recipe, $obs);
            $consult->add();

            header("Location: " . WEBROOT . "medic/index");
        }

        function formMedic(){
            $user = new User();
            $medic = new Medic("", "", "", "", "", "");
            $user = $user->getByUsername($_SESSION['login']);
            $medic = $medic->getByUsername($_SESSION['login']);

            $medic->setPassword($user->getPassword());

            foreach ($medic->getTags() as $key => $value) {
                $this->set($key, $value);
            }

            $this->set("id", $medic->getId());
            
            $this->render("formMedic.html");
        }

        function updateMedic(){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $name = $_POST['name'];
            $address = $_POST['address'];
            $number = $_POST['number'];
            $email = $_POST['email'];
            $specialty = $_POST['specialty'];
            $crm = $_POST['crm'];

            $medic = new Medic($name, $address, $number, $email, $specialty, $crm, $username, $password);
            $medic->update($_POST['id']);

            $_SESSION['login'] = $username;


            header("Location: " . WEBROOT . "medic/index");
        }
    }
?>