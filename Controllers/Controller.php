<?php
    class Controller{
        var $tags = [];
        var $template;

        public function getFile($file){
            if(file_exists($file)){
                $file = file_get_contents($file);
                return $file;
            }else{
                return false;
            }
        }

        public function set($tag, $value){
            $this->tags[$tag] = $value;
        }

        private function getBetween($str, $needle_start, $needle_end) {
            $pos = strpos($str, $needle_start);
            $start = $pos === false ? 0 : $pos + strlen($needle_start);
        
            $pos = strpos($str, $needle_end, $start);
            $end = $start === false ? strlen($str) : $pos;
         
            return substr($str,  $start, $end - $start);
        }

        function replaceBetween($str, $needle_start, $needle_end, $replacement) {
            $pos = strpos($str, $needle_start);
            $start = $pos === false ? 0 : $pos + strlen($needle_start);
        
            $pos = strpos($str, $needle_end, $start);
            $end = $start === false ? strlen($str) : $pos;
         
            return substr_replace($str, $replacement,  $start, $end - $start);
        }
        
        

        private function replaceTags(){
            $loopAux = "";
            $match = "";
            foreach ($this->tags as $tag => $value) {
                if(is_array($value)){
                    $start = '{'.$tag.'}';
                    $end = '{/'.$tag.'}';
                    $match = $this->getBetween(htmlspecialchars($this->template), $start, $end);
                    
                    foreach ($value as $k => $v) {
                        $replaceObject = $match;
                        foreach ($v->getTags() as $objectTag => $objectValue) {
                            $replaceObject = str_replace('{'.$objectTag.'}', $objectValue, $replaceObject);
                        }
                        $loopAux = $loopAux . $replaceObject;
                    }
                    $this->template = $this->replaceBetween(htmlspecialchars($this->template), '{'.$tag.'}', '{/'.$tag.'}',$loopAux);
                    $this->template = str_replace('{'.$tag.'}', "", $this->template);
                    $this->template = str_replace('{/'.$tag.'}', "", $this->template);
                    $this->template = htmlspecialchars_decode($this->template);
                    $loopAux = "";
                    $match = "";
                }else{
                    $this->template = str_replace('{'.$tag.'}', $value, $this->template);
                }
            }
            return true;
        }
        

        function render($filename){
            $this->template = $this->getFile(ROOT . "Views/" . ucfirst(str_replace('Controller', '', get_class($this))) . '/' . $filename); 
            $this->set("ROOT", ROOT);
            $this->replaceTags();

            echo($this->template);
            
        }

    }
?>