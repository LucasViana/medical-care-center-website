<?php
    require_once(ROOT .  "/Models/User.php");
    require_once(ROOT .  "/Controllers/Controller.php");
    
    class indexController extends Controller{
        
        function main(){
            $this->render("main.html");
        }

        function login(){
            $user = new User($_POST['user'], $_POST['password'], "");
            $checkLogin = $user->login($_POST['user'], $_POST['password']);

            if($checkLogin == false){
                //Show error to user
                echo("User not found");
                header("Location: " . WEBROOT . "index/main");
            }else{ 
            
                $_SESSION['login'] = $_POST['user'];
                
                $redirect = userSessionInfo();

                $_SESSION['type'] = $redirect[1];

                header("Location: " . WEBROOT . $redirect[1] ."/index");
            }
        }

        function logout(){
            if(isset($_SESSION['login'])){
                session_unset();
                header("Location: " . WEBROOT . "index/main");
            }
        }
    }
?>