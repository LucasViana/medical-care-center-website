<?php
    require_once(ROOT .  "/Models/Patient.php");
    require_once(ROOT .  "/Models/Medic.php");
    require_once(ROOT .  "/Models/Laboratory.php");
    require_once(ROOT .  "/Controllers/Controller.php");
    
    class adminController extends Controller{
        
        function index(){
            $this->set("username", $_SESSION['login']);
            $this->render("index.html");
        }

        function formLab(){
            $this->set("username", $_SESSION['login']);
            $this->render("formLab.html");
        }

        function registerLab(){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $name = $_POST['name'];
            $address = $_POST['address'];
            $number = $_POST['number'];
            $email = $_POST['email'];
            $type = $_POST['type'];
            $cnpj = $_POST['cnpj'];

            $lab = new Laboratory($name, $address, $number, $email, $type, $cnpj, $username, $password);
            $lab->add();
            
            header("Location: " . WEBROOT . "admin/index");
        }

        function formMedic(){
            $this->set("username", $_SESSION['login']);
            $this->render("formMedic.html");
        }

        function registerMedic(){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $name = $_POST['name'];
            $address = $_POST['address'];
            $number = $_POST['number'];
            $email = $_POST['email'];
            $specialty = $_POST['specialty'];
            $crm = $_POST['crm'];

            $medic = new Medic($name, $address, $number, $email, $specialty, $crm, $username, $password);
            $medic->add();

            header("Location: " . WEBROOT . "admin/index");
        }

        function formPatient(){
            $this->set("username", $_SESSION['login']);
            $this->render("formPatient.html");
        }

        function registerPatient(){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $name = $_POST['name'];
            $address = $_POST['address'];
            $number = $_POST['number'];
            $email = $_POST['email'];
            $gender = $_POST['gender'];
            $age = $_POST['age'];
            $cpf = $_POST['cpf'];

            $patient = new Patient($name, $address, $number, $email, $gender, $age, $cpf, $username, $password);
            $patient->add();

            header("Location: " . WEBROOT . "admin/index");
        }

    }
?>