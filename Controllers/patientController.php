<?php
    require_once(ROOT .  "/Models/User.php");
    require_once(ROOT .  "/Models/Patient.php");
    require_once(ROOT .  "/Models/Medic.php");
    require_once(ROOT .  "/Models/Laboratory.php");
    require_once(ROOT .  "/Models/Consultation.php");
    require_once(ROOT .  "/Models/Exam.php");
    require_once(ROOT .  "/Controllers/Controller.php");
    

    class patientController extends Controller{
        
        function index(){
            $this->render("index.html");
        }


        function consultHistory(){
            $user = new Patient("", "", "", "", "", "", "");
            $consultDao = new Consultation("", "", "", "", "");
            $consults = $consultDao->getByPatient($user->getByUsername($_SESSION['login'])->getId());
    
            $medic = new Medic("", "", "", "", "", "");
            foreach ($consults as $consult) {
                $consult->setMedic($medic->get($consult->getMedic())->getName());
                $consult->setDate(str_replace("T", " ", $consult->getDate()));
            }

            $this->set("consults", $consults);

            $this->render("consultHistory.html");
        }

        function examHistory(){
            $user = new Patient("", "", "", "", "", "", "");
            $examDao = new Exam("", "", "", "", "");
            $exams = $examDao->getByPatient($user->getByUsername($_SESSION['login'])->getId());
            
            $laboratory = new Laboratory("", "", "", "", "", "");
            foreach ($exams as $exam) {
                $exam->setLaboratory($laboratory->get($exam->getLaboratory())->getName());
                $exam->setDate(str_replace("T", " ", $exam->getDate()));
            }

            $this->set("exams", $exams);

            $this->render("examHistory.html");
        }

        function formPatient(){
            $user = new User();
            $patient = new Patient("", "", "", "", "", "", "");

            $user = $user->getByUsername($_SESSION['login']);
            
            $patient = $patient->getByUsername($_SESSION['login']);
            
            $patient->setPassword($user->getPassword());


            foreach ($patient->getTags() as $key => $value) {
                $this->set($key, $value);
            }

            $this->set("id", $patient->getId());

            $this->render("formPatient.html");
        }

        function updatePatient(){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $name = $_POST['name'];
            $address = $_POST['address'];
            $number = $_POST['number'];
            $email = $_POST['email'];
            $gender = $_POST['gender'];
            $age = $_POST['age'];
            $cpf = $_POST['cpf'];

            $patient = new Patient($name, $address, $number, $email, $gender, $age, $cpf, $username, $password);
            $patient->update($_POST['id']);

            $_SESSION['login'] = $username;

            header("Location: " . WEBROOT . "patient/index");
        }
    }
?>