<?php
    require_once(ROOT .  "/Models/User.php");
    require_once(ROOT .  "/Models/Laboratory.php");
    require_once(ROOT .  "/Models/Exam.php");
    require_once(ROOT .  "/Models/Patient.php");
    require_once(ROOT .  "/Controllers/Controller.php");

    class laboratoryController extends Controller{
        
        function index(){
            $this->render("index.html");
        }

        function examHistory(){
            $lab = new Laboratory("", "", "", "", "", "");
            $examDao = new Exam("", "", "", "", "");
            $exams = $examDao->getByLaboratory($lab->getByUsername($_SESSION['login'])->getId());
            
            $patient = new Patient("", "", "", "", "", "", "");
            foreach ($exams as $exam) {
                $exam->setPatient($patient->get($exam->getPatient())->getName());
                $exam->setDate(str_replace("T", " ", $exam->getDate()));
            }
            
            $this->set("exams", $exams);

            $this->render("examHistory.html");
        }

        function formExamUpdate($id){
            $this->set("username", $_SESSION['login']);
            $patientDao = new Patient("", "", "", "", "", "", "");

            $patients = $patientDao->getAll();

            $exam = new Exam("", "", "", "", "");
            $exam = $exam->get($id);

            foreach ($exam->getTags() as $key => $value) {
                $this->set($key, $value);
            }
            
            
            $this->set("patients", $patients);
            $this->render("formExamUpdate.html");
        }

        function updateExam($id){
            $lab = new Laboratory("", "", "", "", "", "");
            $lab = $lab->getByUsername($_SESSION['login'])->getId();
            $patient = $_POST['patient'];
            $exam = $_POST['exam'];
            $result = $_POST['result'];
            $date = $_POST['date'];

            $exam = new Exam($date, $lab, $patient, $exam, $result);

            $exam->update($id);

            header("Location: " . WEBROOT . "laboratory/index");
        }

        function formExam(){
            $this->set("username", $_SESSION['login']);
            $patientDao = new Patient("", "", "", "", "", "", "");

            $patients = $patientDao->getAll();
            
            
            $this->set("patients", $patients);
            $this->render("formExam.html");
        }

        function registerExam(){
            $lab = new Laboratory("", "", "", "", "", "");
            $lab = $lab->getByUsername($_SESSION['login'])->getId();
            $patient = $_POST['patient'];
            $exam = $_POST['exam'];
            $result = $_POST['result'];
            $date = $_POST['date'];

            $exam = new Exam($date, $lab, $patient, $exam, $result);
            
            $exam->add();

            header("Location: " . WEBROOT . "laboratory/index");
        }

        function formLab(){
            $user = new User();
            $laboratory = new Laboratory("", "", "", "", "", "");
            $user = $user->getByUsername($_SESSION['login']);

            $laboratory = $laboratory->getByUsername($_SESSION['login']);
            
            $laboratory->setPassword($user->getPassword());

            foreach ($laboratory->getTags() as $key => $value) {
                $this->set($key, $value);
            }
            $this->set("id", $laboratory->getId());
            
            $this->render("formLab.html");

        }

        function updateLab(){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $name = $_POST['name'];
            $address = $_POST['address'];
            $number = $_POST['number'];
            $email = $_POST['email'];
            $type = $_POST['type'];
            $cnpj = $_POST['cnpj'];
            

            $lab = new Laboratory($name, $address, $number, $email, $type, $cnpj, $username, $password);
            $lab->update($_POST['id']);
            
            $_SESSION['login'] = $username;



            header("Location: " . WEBROOT . "laboratory/index");
        }
    }
?>