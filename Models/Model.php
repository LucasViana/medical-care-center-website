<?php
require_once(ROOT . "Lib/database.php");
abstract class Model{
    protected $database;
    public function __construct() {
        $this->database = db();
    }
    public function get($id){
        $stmt = $this->database->prepare("SELECT * FROM ". strtolower(get_class($this)) . " where id = ?");
        $stmt->execute(array($id));
        $arr = $stmt->fetch(PDO::FETCH_ASSOC);
        return $this->dataToObject($arr);
    }
    abstract public function add();
    abstract public function update($id);
    abstract public function dataToObject($data);
}
?>