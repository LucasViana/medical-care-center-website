<?php
    require_once(ROOT . "Models/" . "User.php");

    class Patient extends User{
        private $name, $address, $phone, $email, $gender, $age, $cpf, $id;
        public function __construct($name, $address, $phone, $email, $gender, $age, $cpf, $username = "", $password = "", $type = "", $id = 0) {
            parent::__construct($username, $password, $type);
            $this->name = $name;
            $this->address = $address;
            $this->phone = $phone;
            $this->email = $email;
            $this->gender = $gender;
            $this->age = $age;
            $this->cpf = $cpf;
            $this->id = $id; 
        
        }
        

        public function getTags(){
            $tags = array(
                "name" => $this->name,
                "address" => $this->address,
                "phone" => $this->phone,
                "email" => $this->email,
                "gender" => $this->gender,
                "age" => $this->age,
                "cpf" => $this->cpf,
                "id" => $this->id,
            );
            $tags = array_merge($tags, parent::getTags());
            return $tags;
        }
        

        public function dataToObject($data){
            return new Patient($data["name"], $data["address"], $data["phone"], $data["email"], $data["gender"], $data["age"], $data["cpf"], $data["username"], "", "patient", $data["id"]);
        }



        public function add(){
            $this->setType("patient");
            parent::add();
            $stmt = $this->database->prepare("INSERT INTO patient (name, address, phone, email, gender, age, cpf, username) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->execute(array($this->name, $this->address, $this->phone, $this->email, $this->gender, $this->age, $this->cpf, $this->username));
        }
        public function update($id){
            parent::update($_SESSION['login']);
            $stmt= $this->database->prepare("UPDATE patient SET name=?, address=?, phone=?, email=?, gender=?,  age=?, cpf=?, username=? WHERE id=?");
            $stmt->execute(array($this->name, $this->address, $this->phone, $this->email, $this->gender, $this->age, $this->cpf, $this->username, $id));
        }
        public function getByCPF($cpf){
            $stmt = $this->database->prepare("SELECT * FROM patient where cpf = ?");
            $stmt->execute(array($cpf));
            $arr = $stmt->fetch(PDO::FETCH_ASSOC);
            return $this->dataToObject($arr);
        }

        public function get($id){
            $stmt = $this->database->prepare("SELECT * FROM patient where id = ?");
            $stmt->execute(array($id));
            $arr = $stmt->fetch(PDO::FETCH_ASSOC);
            return $this->dataToObject($arr);
        }
        
        public function getAll(){
            $stmt = $this->database->prepare("SELECT * from patient");
            $stmt->execute();
            $returnArr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $arr = array();
            foreach ($returnArr as $key => $value) {
                array_push($arr, $this->dataToObject($value));
            }
            return $arr;
        }



        public function getId()
        {
                return $this->id;
        }

        /**
         * Get the value of name
         */ 
        public function getName()
        {
                return $this->name;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */ 
        public function setName($name)
        {
                $this->name = $name;

                return $this;
        }

        /**
         * Get the value of address
         */ 
        public function getAddress()
        {
                return $this->address;
        }

        /**
         * Set the value of address
         *
         * @return  self
         */ 
        public function setAddress($address)
        {
                $this->address = $address;

                return $this;
        }

        /**
         * Get the value of phone
         */ 
        public function getPhone()
        {
                return $this->phone;
        }

        /**
         * Set the value of phone
         *
         * @return  self
         */ 
        public function setPhone($phone)
        {
                $this->phone = $phone;

                return $this;
        }

        /**
         * Get the value of email
         */ 
        public function getEmail()
        {
                return $this->email;
        }

        /**
         * Set the value of email
         *
         * @return  self
         */ 
        public function setEmail($email)
        {
                $this->email = $email;

                return $this;
        }

        /**
         * Get the value of gender
         */ 
        public function getGender()
        {
                return $this->gender;
        }

        /**
         * Set the value of gender
         *
         * @return  self
         */ 
        public function setGender($gender)
        {
                $this->gender = $gender;

                return $this;
        }

        /**
         * Get the value of age
         */ 
        public function getAge()
        {
                return $this->age;
        }

        /**
         * Set the value of age
         *
         * @return  self
         */ 
        public function setAge($age)
        {
                $this->age = $age;

                return $this;
        }

        /**
         * Get the value of cpf
         */ 
        public function getCpf()
        {
                return $this->cpf;
        }

        /**
         * Set the value of cpf
         *
         * @return  self
         */ 
        public function setCpf($cpf)
        {
                $this->cpf = $cpf;

                return $this;
        }
    }
?>