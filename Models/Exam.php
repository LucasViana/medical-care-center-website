<?php
    
    class Exam extends Model{
        private $date, $laboratory, $patient, $examType, $result, $id;
        public function __construct($date, $laboratory, $patient, $examType, $result, $id = -1) {
            parent::__construct();
            $this->date = $date;
            $this->laboratory = $laboratory;
            $this->patient = $patient;
            $this->examType = $examType;
            $this->result = $result;
            $this->id = $id;
        }
        

        public function getTags(){
            $tags = array(
                "id" => $this->id,
                "date" => $this->date,
                "laboratory" => $this->laboratory,
                "patient" => $this->patient,
                "examType" => $this->examType,
                "result" => $this->result,
            );
            return $tags;
        }

        public function dataToObject($data){
            return new Exam($data["date"], $data["labId"], $data["patientId"], $data["examType"], $data["result"], $data["id"]);
        }

        public function getByPatient($id){
            $stmt = $this->database->prepare("SELECT * FROM exam where patientId = ?");
            $stmt->execute(array($id));   
            $returnArr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $arr = array();
            foreach ($returnArr as $key => $value) {
                array_push($arr, $this->dataToObject($value));
            }
            return $arr;
        }
        public function getByLaboratory($id){
            $stmt = $this->database->prepare("SELECT * FROM exam where labId = ?");
            $stmt->execute(array($id));
            $returnArr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $arr = array();
            foreach ($returnArr as $key => $value) {
                array_push($arr, $this->dataToObject($value));
            }
            return $arr;
        }

        // TODO: Figure out how make foreign key on PHP.
        public function add(){
            $stmt = $this->database->prepare("INSERT INTO exam (date, labId, patientId, examType, result) VALUES (?, ?, ?, ?, ?)");
            $stmt->execute(array($this->date, $this->laboratory, $this->patient, $this->examType, $this->result));
        }

        public function update($id){
            $stmt= $this->database->prepare("UPDATE exam SET date=?, labId=?, patientId=?, examType=?, result=? WHERE id=?");
            $stmt->execute(array($this->date, $this->laboratory, $this->patient, $this->examType, $this->result, $id));
        }

        


        /**
         * Get the value of date
         */ 
        public function getDate()
        {
                return $this->date;
        }

        /**
         * Set the value of date
         *
         * @return  self
         */ 
        public function setDate($date)
        {
                $this->date = $date;

                return $this;
        }

        /**
         * Get the value of laboratory
         */ 
        public function getLaboratory()
        {
                return $this->laboratory;
        }

        /**
         * Set the value of laboratory
         *
         * @return  self
         */ 
        public function setLaboratory($laboratory)
        {
                $this->laboratory = $laboratory;

                return $this;
        }

        /**
         * Get the value of patient
         */ 
        public function getPatient()
        {
                return $this->patient;
        }

        /**
         * Set the value of patient
         *
         * @return  self
         */ 
        public function setPatient($patient)
        {
                $this->patient = $patient;

                return $this;
        }

        /**
         * Get the value of examType
         */ 
        public function getType()
        {
                return $this->examType;
        }

        /**
         * Set the value of examType
         *
         * @return  self
         */ 
        public function setType($examType)
        {
                $this->examType = $examType;

                return $this;
        }

        /**
         * Get the value of result
         */ 
        public function getResult()
        {
                return $this->result;
        }

        /**
         * Set the value of result
         *
         * @return  self
         */ 
        public function setResult($result)
        {
                $this->result = $result;

                return $this;
        }
    }
    
?>