<?php
    require_once(ROOT . "Models/" . "User.php");
    
    class Laboratory extends User{
        private $name, $address, $phone, $email, $examType, $cnpj, $id;
        public function __construct($name, $address, $phone, $email, $examType, $cnpj, $username = "", $password = "", $userType = "laboratory", $id = 0) {
            parent::__construct($username, $password, $userType);
            $this->name = $name;
            $this->address = $address;
            $this->phone = $phone;
            $this->email = $email;
            $this->examType = $examType;
            $this->cnpj = $cnpj;
            $this->id = $id;
        }
        

        public function getTags(){
            $tags = array(
                "name" => $this->name,
                "address" => $this->address,
                "phone" => $this->phone,
                "email" => $this->email,
                "examType" => $this->examType,
                "cnpj" => $this->cnpj,
                "id" => $this->id,
            );
            $tags = array_merge($tags, parent::getTags());
            return $tags;
        }

        public function dataToObject($data){
            return new Laboratory($data["name"], $data["address"], $data["phone"], $data["email"], $data["examType"], $data["cnpj"], $data["username"], "", "laboratory", $data["id"]);
        }

        public function add(){
            $this->setType("laboratory");
            parent::add();
            $stmt = $this->database->prepare("INSERT INTO laboratory (name, address, phone, email, examType, cnpj, username) VALUES (?, ?, ?, ?, ?, ?, ?)");
            $stmt->execute(array($this->name, $this->address, $this->phone, $this->email, $this->examType, $this->cnpj, $this->username));
        }
        public function update($id){
            parent::update($_SESSION['login']);
            $stmt= $this->database->prepare("UPDATE laboratory SET name=?, address=?, phone=?, email=?, examType=?,  cnpj=?, username=? WHERE id=?");
            $stmt->execute(array($this->name, $this->address, $this->phone, $this->email, $this->examType, $this->cnpj, $this->username, $id));
        }
        public function getByCNPJ($cnpj){
            $stmt = $this->database->prepare("SELECT * FROM laboratory where cnpj = ?");
            $stmt->execute(array($cnpj));
            $arr = $stmt->fetch(PDO::FETCH_ASSOC);
            return $this->dataToObject($arr);
        }

        public function get($id){
            $stmt = $this->database->prepare("SELECT * FROM laboratory where id = ?");
            $stmt->execute(array($id));
            $arr = $stmt->fetch(PDO::FETCH_ASSOC);
            return $this->dataToObject($arr);
        }

        public function getId()
        {
                return $this->id;
        }

        /**
         * Get the value of name
         */ 
        public function getName()
        {
                return $this->name;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */ 
        public function setName($name)
        {
                $this->name = $name;

                return $this;
        }

        /**
         * Get the value of address
         */ 
        public function getAddress()
        {
                return $this->address;
        }

        /**
         * Set the value of address
         *
         * @return  self
         */ 
        public function setAddress($address)
        {
                $this->address = $address;

                return $this;
        }

        /**
         * Get the value of phone
         */ 
        public function getPhone()
        {
                return $this->phone;
        }

        /**
         * Set the value of phone
         *
         * @return  self
         */ 
        public function setPhone($phone)
        {
                $this->phone = $phone;

                return $this;
        }

        /**
         * Get the value of email
         */ 
        public function getEmail()
        {
                return $this->email;
        }

        /**
         * Set the value of email
         *
         * @return  self
         */ 
        public function setEmail($email)
        {
                $this->email = $email;

                return $this;
        }

        /**
         * Get the value of examType
         */ 
        public function getType()
        {
                return $this->examType;
        }

        /**
         * Set the value of examType
         *
         * @return  self
         */ 
        public function setType($examType)
        {
                $this->examType = $examType;

                return $this;
        }

        /**
         * Get the value of cnpj
         */ 
        public function getCnpj()
        {
                return $this->cnpj;
        }

        /**
         * Set the value of cnpj
         *
         * @return  self
         */ 
        public function setCnpj($cnpj)
        {
                $this->cnpj = $cnpj;

                return $this;
        }
    }
?>