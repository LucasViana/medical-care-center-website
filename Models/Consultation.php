<?php
    class Consultation extends Model{
        private $date, $medic, $patient, $recipe, $observations, $id;
        
        public function __construct($date, $medic, $patient, $recipe, $observations, $id = -1) {
            parent::__construct();
            $this->date = $date;
            $this->medic = $medic;
            $this->patient = $patient;
            $this->recipe = $recipe;
            $this->observations = $observations;
            $this->id = $id;
        }
        
        

        public function getTags(){
            $tags = array(
                "id" => $this->id,
                "date" => $this->date,
                "medic" => $this->medic,
                "patient" => $this->patient,
                "recipe" => $this->recipe,
                "observations" => $this->observations,
            );
            return $tags;
        }

        public function dataToObject($data){
            return new Consultation($data["date"], $data["medicId"], $data["patientId"], $data["recipe"], $data["observations"], $data["id"]);
            
        }

        public function getByMedic($id){
            $stmt = $this->database->prepare("SELECT * FROM consultation where medicId = ?");
            $stmt->execute(array($id)); 
            $returnArr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $arr = array();
            foreach ($returnArr as $key => $value) {
                array_push($arr, $this->dataToObject($value));
            }
            return $arr;
        }
        public function getByPatient($id){
            $stmt = $this->database->prepare("SELECT * FROM consultation where patientId = ?");
            $stmt->execute(array($id)); 
            $returnArr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $arr = array();
            foreach ($returnArr as $key => $value) {
                array_push($arr, $this->dataToObject($value));
            }
            return $arr;
        }
        public function add(){
            $stmt = $this->database->prepare("INSERT INTO consultation (date, medicId, patientId, recipe, observations) VALUES (?, ?, ?, ?, ?)");
            $stmt->execute(array($this->date, $this->medic, $this->patient, $this->recipe, $this->observations));
        }


        public function update($id){
            $stmt= $this->database->prepare("UPDATE consultation SET date=?, medicId=?, patientId=?, recipe=?, observations=? WHERE id=?");
            $stmt->execute(array($this->date, $this->medic, $this->patient, $this->recipe, $this->observations, $id));
        
        }


        /**
         * Get the value of observations
         */ 
        public function getObservations()
        {
                return $this->observations;
        }

        /**
         * Set the value of observations
         *
         * @return  self
         */ 
        public function setObservations($observations)
        {
                $this->observations = $observations;

                return $this;
        }

        /**
         * Get the value of date
         */ 
        public function getDate()
        {
                return $this->date;
        }

        /**
         * Set the value of date
         *
         * @return  self
         */ 
        public function setDate($date)
        {
                $this->date = $date;

                return $this;
        }

        /**
         * Get the value of medic
         */ 
        public function getMedic()
        {
                return $this->medic;
        }

        /**
         * Set the value of medic
         *
         * @return  self
         */ 
        public function setMedic($medic)
        {
                $this->medic = $medic;

                return $this;
        }

        /**
         * Get the value of patient
         */ 
        public function getPatient()
        {
                return $this->patient;
        }

        /**
         * Set the value of patient
         *
         * @return  self
         */ 
        public function setPatient($patient)
        {
                $this->patient = $patient;

                return $this;
        }

        /**
         * Get the value of recipe
         */ 
        public function getReceipt()
        {
                return $this->recipe;
        }

        /**
         * Set the value of recipe
         *
         * @return  self
         */ 
        public function setReceipt($recipe)
        {
                $this->recipe = $recipe;

                return $this;
        }
    }
    
?>