<?php
    require_once(ROOT . "Models/" . "User.php");
    

    class Medic extends User{
        private $name, $address, $phone, $email, $area, $crm, $id;
        public function __construct($name, $address, $phone, $email, $area, $crm, $username = "", $password = "", $type = "", $id = 0) {
            parent::__construct($username, $password, "medic");
            $this->name = $name;
            $this->address = $address;
            $this->phone = $phone;
            $this->email = $email;
            $this->area = $area;
            $this->crm = $crm;
            $this->id = $id;
        }
       

        public function getTags(){
            $tags = array(
                "name" => $this->name,
                "address" => $this->address,
                "phone" => $this->phone,
                "email" => $this->email,
                "area" => $this->area,
                "crm" => $this->crm,
                "id" => $this->id,
            );
            $tags = array_merge($tags, parent::getTags());
            return $tags;
        }

        public function dataToObject($data){
            return new Medic($data["name"], $data["address"], $data["phone"], $data["email"], $data["area"], $data["crm"], $data["username"], "", "medic", $data["id"]);
        }
        //TODO: Check for uniqueness
        public function add(){
            $this->setType("medic");
            parent::add();
            $stmt = $this->database->prepare("INSERT INTO medic (name, address, phone, email, area, crm, username) VALUES (?, ?, ?, ?, ?, ?, ?)");
            $stmt->execute(array($this->name, $this->address, $this->phone, $this->email, $this->area, $this->crm, $this->username));
        }

        public function getByCRM($crm){
            $stmt = $this->database->prepare("SELECT * FROM medic where crm = ?");
            $stmt->execute(array($crm));
            $arr = $stmt->fetch(PDO::FETCH_ASSOC);
            return $this->dataToObject($arr);  
        }

        public function get($id){
            $stmt = $this->database->prepare("SELECT * FROM medic where id = ?");
            $stmt->execute(array($id));
            $arr = $stmt->fetch(PDO::FETCH_ASSOC);
            return $this->dataToObject($arr);
        }

        public function update($id){
            parent::update($_SESSION['login']);
            $stmt= $this->database->prepare("UPDATE medic SET name=?, address=?, phone=?, email=?, area=?, crm=?, username=? WHERE id=?");
            $stmt->execute(array($this->name, $this->address, $this->phone, $this->email, $this->area, $this->crm, $this->username, $id));
        }
        

        public function getId()
        {
                return $this->id;
        }
        /**
         * Get the value of name
         */ 
        public function getName()
        {
                return $this->name;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */ 
        public function setName($name)
        {
                $this->name = $name;

                return $this;
        }

        /**
         * Get the value of address
         */ 
        public function getAddress()
        {
                return $this->address;
        }

        /**
         * Set the value of address
         *
         * @return  self
         */ 
        public function setAddress($address)
        {
                $this->address = $address;

                return $this;
        }

        /**
         * Get the value of phone
         */ 
        public function getPhone()
        {
                return $this->phone;
        }

        /**
         * Set the value of phone
         *
         * @return  self
         */ 
        public function setPhone($phone)
        {
                $this->phone = $phone;

                return $this;
        }

        /**
         * Get the value of email
         */ 
        public function getEmail()
        {
                return $this->email;
        }

        /**
         * Set the value of email
         *
         * @return  self
         */ 
        public function setEmail($email)
        {
                $this->email = $email;

                return $this;
        }

        /**
         * Get the value of area
         */ 
        public function getArea()
        {
                return $this->area;
        }

        /**
         * Set the value of area
         *
         * @return  self
         */ 
        public function setArea($area)
        {
                $this->area = $area;

                return $this;
        }

        /**
         * Get the value of crm
         */ 
        public function getCrm()
        {
                return $this->crm;
        }

        /**
         * Set the value of crm
         *
         * @return  self
         */ 
        public function setCrm($crm)
        {
                $this->crm = $crm;

                return $this;
        }
    }
    
?>