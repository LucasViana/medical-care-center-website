<?php
    require_once(ROOT . "Models/" . "Model.php");
    class User extends Model{
        protected $username, $password, $type;
        public function __construct($username = "", $password = "", $type = "") {
            parent::__construct();
            $this->username = $username;
            $this->password = $password;
            $this->type = $type;
        }
        

        public function getTags(){
            $tags = array(
                "username" => $this->username,
                "password" => $this->password,
                "type" => $this->type,
            );
            return $tags;
        }

        public function dataToObject($data){
            return new User($data["username"], $data["password"], $data["type"]);
        }

        public function add(){
            $stmt = $this->database->prepare("INSERT INTO user (username, password, type) VALUES (?, ?, ?)");
            $stmt->execute(array($this->username, $this->password, $this->type));
        }

        public function update($username){
            $stmt= $this->database->prepare("UPDATE user SET username=?, password=?, type=? WHERE username=?");
            $stmt->execute(array($this->username, $this->password, $this->type, $username));
        }


        public function login($username, $password){
            $stmt = $this->database->prepare("SELECT * from user where username = ? and password = ?");
            $stmt->execute(array($username, $password));
            
            $arr = $stmt->fetch(PDO::FETCH_ASSOC);
            if(empty($arr)){
                return false;
            }
            return true;
        }


        public function getByUsername($username){
            $stmt = $this->database->prepare("SELECT * FROM ". strtolower(get_class($this)) . " where username = ?");
            $stmt->execute(array($username));
            $arr = $stmt->fetch(PDO::FETCH_ASSOC);
            
            return $this->dataToObject($arr);
        }

    

        /**
         * Get the value of username
         */ 
        public function getUsername()
        {
                return $this->username;
        }

        public function getType()
        {
                return $this->type;
        }

        /**
         * Set the value of username
         *
         * @return  self
         */ 
        public function setUsername($username)
        {
                $this->username = $username;

                return $this;
        }
        public function setType($type)
        {
                $this->type = $type;

                return $this;
        }

        /**
         * Get the value of password
         */ 
        public function getPassword()
        {
                return $this->password;
        }

        /**
         * Set the value of password
         *
         * @return  self
         */ 
        public function setPassword($password)
        {
                $this->password = $password;

                return $this;
        }

      

    }
?>