<?php
try{
    $db = new PDO('mysql:host=localhost;port=3306;dbname=mcc', 'root', '');
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

$cnpj = $_POST['cnpj'];
$str = "SELECT * FROM laboratory where cnpj = ?";
if(isset($_POST['id'])){
    $str = $str . "and id != ?";
    $stmt = $db->prepare($str);
    $stmt->execute(array($cnpj, $_POST['id']));
}else{
    $stmt = $db->prepare($str);
    $stmt->execute(array($cnpj));
}

$arr = $stmt->fetch(PDO::FETCH_ASSOC);
if(empty($arr)){
    echo(false);
}else{
    echo(true);
}


?>