<?php
try{
    $db = new PDO('mysql:host=localhost;port=3306;dbname=mcc', 'root', '');
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

$cpf = $_POST['cpf'];

$str = "SELECT * FROM patient where cpf = ?";
if(isset($_POST['id'])){
    $str = $str . "and id != ?";
    $stmt = $db->prepare($str);
    $stmt->execute(array($cpf, $_POST['id']));
}else{
    $stmt = $db->prepare($str);
    $stmt->execute(array($cpf));
}

$arr = $stmt->fetch(PDO::FETCH_ASSOC);
if(empty($arr)){
    echo(false);
}else{
    echo(true);
}


?>