-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 04-Out-2019 às 20:53
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mcc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `consultation`
--

CREATE TABLE `consultation` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `recipe` varchar(255) NOT NULL,
  `observations` varchar(255) NOT NULL,
  `medicId` int(11) NOT NULL,
  `patientId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `consultation`
--

INSERT INTO `consultation` (`id`, `date`, `recipe`, `observations`, `medicId`, `patientId`) VALUES
(1, '2019-10-05', 'parace', 'foi', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `exam`
--

CREATE TABLE `exam` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `examType` varchar(255) NOT NULL,
  `result` varchar(255) NOT NULL,
  `labId` int(11) NOT NULL,
  `patientId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `exam`
--

INSERT INTO `exam` (`id`, `date`, `examType`, `result`, `labId`, `patientId`) VALUES
(1, '2019-10-26', 'qlq', 'ainda nao', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `laboratory`
--

CREATE TABLE `laboratory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `examType` varchar(255) NOT NULL,
  `cnpj` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `laboratory`
--

INSERT INTO `laboratory` (`id`, `name`, `address`, `phone`, `email`, `examType`, `cnpj`, `username`) VALUES
(1, 'labaaaaa', 'lab', '12344', 'lab@furg.br', 'lab', '1234125', 'laba'),
(2, 'lab2', 'lab2', '12134564654', 'emaillab2@email.com', 'laboratory', '1111122222', 'lab2'),
(3, 'nome3', 'end3', 'tel', 'asd@abc.com', 'laboratory', 'ad', 'lab3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `medic`
--

CREATE TABLE `medic` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `crm` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `medic`
--

INSERT INTO `medic` (`id`, `name`, `address`, `phone`, `email`, `area`, `crm`, `username`) VALUES
(1, 'nome', 'medic', '1234541246441265', 'email@email.com', 'espece', 'as16565', 'medico');

-- --------------------------------------------------------

--
-- Estrutura da tabela `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` enum('Masculino','Feminino') NOT NULL,
  `age` tinyint(3) UNSIGNED NOT NULL,
  `cpf` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `patient`
--

INSERT INTO `patient` (`id`, `address`, `name`, `phone`, `email`, `gender`, `age`, `cpf`, `username`) VALUES
(1, 'patient', 'patient', '12345678', 'emailPatient@furg.br', 'Masculino', 22, '111111', 'patiente'),
(22, 'endDummy', 'dummy', '21253314', 'dummy@dummy.com', 'Masculino', 11, '02877527042', 'dummy');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` enum('admin','patient','medic','laboratory') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`username`, `password`, `type`) VALUES
('admin', 'admin', 'admin'),
('dummy', '1234', 'patient'),
('lab2', 'lab2', 'laboratory'),
('lab3', 'lab3', 'laboratory'),
('laba', 'laba', 'laboratory'),
('lucasviana', '1234', 'patient'),
('medico', 'medic', 'medic'),
('patiente', 'patient', 'patient');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consultation`
--
ALTER TABLE `consultation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_medic` (`medicId`),
  ADD KEY `fk_patient` (`patientId`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lab` (`labId`),
  ADD KEY `fk_patient_exam` (`patientId`);

--
-- Indexes for table `laboratory`
--
ALTER TABLE `laboratory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cnpj` (`cnpj`),
  ADD KEY `fk_lab_username` (`username`);

--
-- Indexes for table `medic`
--
ALTER TABLE `medic`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `crm` (`crm`),
  ADD KEY `fk_medic_username` (`username`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cpf` (`cpf`),
  ADD KEY `fk_patient_username` (`username`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consultation`
--
ALTER TABLE `consultation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `laboratory`
--
ALTER TABLE `laboratory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `medic`
--
ALTER TABLE `medic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `consultation`
--
ALTER TABLE `consultation`
  ADD CONSTRAINT `fk_medic_consultation` FOREIGN KEY (`medicId`) REFERENCES `medic` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_patient_consultation` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `exam`
--
ALTER TABLE `exam`
  ADD CONSTRAINT `fk_lab_exam` FOREIGN KEY (`labId`) REFERENCES `laboratory` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_patient_exam` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `laboratory`
--
ALTER TABLE `laboratory`
  ADD CONSTRAINT `fk_lab_username` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `medic`
--
ALTER TABLE `medic`
  ADD CONSTRAINT `fk_medic_username` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `patient`
--
ALTER TABLE `patient`
  ADD CONSTRAINT `fk_patient_username` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
