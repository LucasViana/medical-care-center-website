<?php
require_once(ROOT .  "/Models/User.php");

session_start();

function userSessionInfo(){
    $tplFile = "";
    $type = "";
    if(isset($_SESSION['login'])){
        $userModel = new User();
        
        $user = $userModel->getByUsername($_SESSION['login']);
    
        if($user->getType() == "medic"){
            $tplFile = "medic/medic.html";
            $type = "medic";

        }elseif ($user->getType() == "admin") {
            $tplFile = "admin/admin.html";
            $type = "admin";
            
        }elseif ($user->getType() == "patient") {
            $tplFile = "patient/patient.html";
            $type = "patient";
    
        }elseif ($user->getType() == "laboratory"){
            $tplFile = "laboratory/laboratory.html";
            $type = "laboratory";
        }
    }
    return array($tplFile, $type);
}


?>