<?php
    
    function db(){
    static $db = null;
        if ($db === null) {
            try{
                $db = new PDO('mysql:host=localhost;port=3306;dbname=mcc', 'root', '');
            } catch (PDOException $e) {
                print "Error!: " . $e->getMessage() . "<br/>";
                die();
            }
        }
        return $db;
    }
?>