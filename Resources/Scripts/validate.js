
$('#cpf').change(function() {
    var cpf = document.getElementById("cpf");
    if(!validateCPF(cpf.value)){
        cpf.setCustomValidity("CPF inválido");
    }else if(!checkCPF(cpf.value)){
        cpf.setCustomValidity("CPF já cadastrado");
    }else{
        cpf.setCustomValidity("");
    }
});

$('#username').change(function() {
    var username = document.getElementById("username");
   if(!checkUsername(username.value)){
        username.setCustomValidity("Username já cadastrado");
    }else{
        username.setCustomValidity("");
    }
});

$('#crm').change(function() {
    var crm = document.getElementById("crm");
   if(!checkCRM(crm.value)){
        crm.setCustomValidity("CRM já cadastrado");
    }else{
        crm.setCustomValidity("");
    }
});

$('#cnpj').change(function() {
    var cnpj = document.getElementById("cnpj");
    if(!validateCNPJ(cnpj.value)){
        cnpj.setCustomValidity("CNPJ inválido");
    }else if(!checkCNPJ(cnpj.value)){
        cnpj.setCustomValidity("CNPJ já cadastrado");
    }else{
        cnpj.setCustomValidity("");
    }
});





function checkUsername(username, id=-1){
    var id = document.getElementById("id");
    var str = "username=" + username;
    var type = "";
    if(id != null){
        str += "&id=" + id.value;
        //Find user type
        if(document.getElementById("crm") != null){
            type = "medic";
        }else if(document.getElementById("cnpj") != null){
            type = "laboratory";
        }else{
            type = "patient";
        }
        str += "&type=" + type;
    }
    $.ajax({
        url: "../../AJAX/checkUsername.php",
        type: "POST",
        data: str,
        dataType: "html"
    
    }).done(function(resposta) {
        if(resposta=="1"){
            return true;
        }else{
            document.getElementById("username").setCustomValidity("");
            return false;
        }
    
    }).fail(function(jqXHR, textStatus ) {
        console.log("Request failed: " + textStatus);
    
    }).always(function() {
        //Done
    });
}

function checkCPF(cpf, id=-1){
    var id = document.getElementById("id");
    var str = "cpf=" + cpf;
    if(id != null){
        str += "&id=" + id.value;
    }
    $.ajax({
        url: "../../AJAX/checkCPF.php",
        type: "POST",
        data: str,
        dataType: "html"
    
    }).done(function(resposta) {
        if(resposta=="1"){
            return true;
        }else{
            document.getElementById("cpf").setCustomValidity("");
            return false;

        }
    
    }).fail(function(jqXHR, textStatus ) {
        console.log("Request failed: " + textStatus);
    
    }).always(function() {
        //Done
    });
}

function checkCRM(crm, id=-1){
    var id = document.getElementById("id");
    var str = "crm=" + crm;
    if(id != null){
        str += "&id=" + id.value;
    }
    $.ajax({
        url: "../../AJAX/checkCRM.php",
        type: "POST",
        data: str,
        dataType: "html"
    
    }).done(function(resposta) {
        if(resposta=="1"){
            return true;
        }else{
            document.getElementById("crm").setCustomValidity("");
            return false;
        }
    
    }).fail(function(jqXHR, textStatus ) {
        console.log("Request failed: " + textStatus);
    
    }).always(function() {
        //Done
    });
}

function checkCNPJ(cnpj, id=-1){
    var id = document.getElementById("id");
    var str = "cnpj=" + cnpj;
    if(id != null){
        str += "&id=" + id.value;
    }
    $.ajax({
        url: "../../AJAX/checkCNPJ.php",
        type: "POST",
        data: str,
        dataType: "html"
    
    }).done(function(resposta) {
        if(resposta=="1"){
            return true;
        }else{
            document.getElementById("cnpj").setCustomValidity("");
            return false;
        }
    
    }).fail(function(jqXHR, textStatus ) {
        console.log("Request failed: " + textStatus);
    
    }).always(function() {
        
    });
}




function validateCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g,'');	
	if(cpf == '') return false;	
	if (cpf.length != 11 || 
		cpf == "00000000000" || 
		cpf == "11111111111" || 
		cpf == "22222222222" || 
		cpf == "33333333333" || 
		cpf == "44444444444" || 
		cpf == "55555555555" || 
		cpf == "66666666666" || 
		cpf == "77777777777" || 
		cpf == "88888888888" || 
		cpf == "99999999999")
			return false;	
	add = 0;	
	for (i=0; i < 9; i ++)		
		add += parseInt(cpf.charAt(i)) * (10 - i);	
		rev = 11 - (add % 11);	
		if (rev == 10 || rev == 11)		
			rev = 0;	
		if (rev != parseInt(cpf.charAt(9)))		
			return false;
	add = 0;	
	for (i = 0; i < 10; i ++)		
		add += parseInt(cpf.charAt(i)) * (11 - i);	
	rev = 11 - (add % 11);	
	if (rev == 10 || rev == 11)	
		rev = 0;	
	if (rev != parseInt(cpf.charAt(10)))
		return false;		
	return true;
}

function validateCNPJ(cnpj) {
 
    cnpj = cnpj.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
    
}
