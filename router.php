<?php
    class Router{

        static public function parse($url, $request){
            $url = trim($url);
            $explode_url = explode('/', $url);
            $explode_url = array_slice($explode_url, 2);

            
            if(isset($_SESSION['login'])){
                if($_SESSION['type'] != $explode_url[0] && $explode_url[1] != "logout"){
                    header("Location: " . WEBROOT . $_SESSION['type'] ."/index");
                    return;
                }
            }else{
                if ($url == "/MCC/" || $explode_url[0] != "index"){
                    header("Location: " . WEBROOT . "index/main");
                    return;
                }       
            }
            $request->controller = $explode_url[0];
            $request->action = $explode_url[1];
            $request->params = array_slice($explode_url, 2);

        }
    }
?>